<?php include 'php/conexion.php'; ?>
<!DOCTYPE html>
<?php include 'include/head.html';?>
<body>
<?php include 'include/menu.php'; ?>
<?php    
    $dni = $_SESSION["CI_RIF"];
    $sql = "SELECT * FROM pedidos INNER JOIN usuarios ON pedidos.CI_RIF=usuarios.CI_RIF WHERE pedidos.CI_RIF = '$dni' ORDER BY ID_PEDIDO ASC";
    $consulta=mysqli_query($conexion, $sql);
?>

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h3><p align="center">Consulte su pedido</p></h3>
                <div class="datagrid">
                    <table id="listado">
                            <thead>
                            <tr>
                                <th class="noped">N°</th>
                                <th class="noped">Fecha de Pedido</th>
                                <th class="noped">Solicitante</th>
                                <th class="noped">Estatus</th>
                            </tr>
                            </thead>
                            <?php
                            while($resgistros=mysqli_fetch_array($consulta)) {
                                    echo'<tr>
                                            <td class="noped"><a class="btn btn-primary btn-xs" href="consultardetallespedido.php?id='.$resgistros["ID_PEDIDO"].'">'.$resgistros["ID_PEDIDO"].'</a></td>
                                            <td class="noped">'.$resgistros["FECHA_PEDIDO"].'</td>
                                            <td class="noped">'.$resgistros["NOMBRE_USUARIOS"].'</td>
                                            <td class="noped">'.$resgistros["ID_ESTADO"].'</td>
                                        </tr>';
                                    }
                            ?>
                    </table>
                </div>
            </div>
        </div>
        <hr>
    </div>
        <!-- Footer -->
<?php include 'include/piedepagina.php'; ?>
    </div>
<?php include'include/script.html'; ?>
</body>
</html>