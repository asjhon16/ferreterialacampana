<?php  include "php/validariniciodesesion.php"; /*?>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="content-wrapper">
          
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand pd-0" href="index">
                <img src="../images/logos/logo-diapo.png" alt="ferretería de calidad en maracay" height="100%">
                </a>
            </div>
           
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                 <ul class="nav navbar-nav navbar-letf" class="dropdown-menu">
                    <li>
                        <a href="index">Inicio</a>
                    </li>
                    <?php
                    if ($_SESSION['TIPO_USER'] == 1 or $_SESSION['TIPO_USER'] == 2){ 
                    ?>
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-table" aria-hidden="true"></i>
                            Tablas
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                          <li><a href="productos">Articulos</a></li>
                          <li><a href="categorias">Categorias</a></li>
                          <li><a href="estados">Estado de Pedido</a></li>
                          <li><a href="proveedores">Proveedores</a></li>
                        </ul>
                    </li>
                    <?php
                    }
                    ?>
                    
                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-tasks" aria-hidden="true"></i>
                            Procesos
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                        <?php
                        if ($_SESSION['TIPO_USER'] == 1 or $_SESSION['TIPO_USER'] == 3){
                        ?>
                            <li><a href="pedido">Pedido</a></li>
                        <?php
                        }
                        ?>
                        <?php
                        if ($_SESSION['TIPO_USER'] == 1 or $_SESSION['TIPO_USER'] == 2) {                             
                        ?>
                            <li><a href="requisicion">Requisicion</a></li>
                            <li><a href="recepcion">Recepcion</a></li>
                            <li><a href="consultarestadopedido">Estatus de Pedido</a></li>       
                            <li><a href="movimiento_inventario">Movimiento Inventario</a></li>        
                        <?php
                        }
                        ?>
                        </ul>
                    </li>


                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-list-alt" aria-hidden="true"></i>
                            Listados
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                        <?php
                        if ($_SESSION['TIPO_USER'] == 1 or $_SESSION['TIPO_USER'] == 2) {                             
                        ?>
                            <li><a href="productos_list">Articulos</a></li>
                            <li><a href="user_list" >Usuarios</a></li>
                            <li><a href="consultarestados">Estados</a></li>
                            <li><a href="consultarproveedores">Proveedores</a></li>
                            <li><a href="consultarcategorias">Categorias</a></li>
                            <li><a href="consultarclientes">Clientes</a></li>
                        <?php
                        }
                        ?>
                        <?php
                        if ($_SESSION['TIPO_USER'] == 1 or $_SESSION['TIPO_USER'] == 3) {                             
                        ?>  
                          <li><a href="consultarpedidos">Pedidos</a></li>
                        <?php
                        }
                        ?>
                        </ul>
                    </li>

                    <?php
                    if ($_SESSION['TIPO_USER'] == 1) {                             
                    ?>

                    <li class="dropdown">
                        <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-gear" aria-hidden="true"></i>
                            Mantenimiento
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="respaldarbasededatos">Respaldar datos</a></li>
                            <li><a href="restaurarbasededatos">Restaurar datos</a></li>
                            <li><a href="usuarios" >Usuarios</a></li>
                            <li><a href="auditoria_usuarios">Auditoria Usuarios</a></li>
                        </ul>
                    </li>
                    <?php
                    }
                    ?>
                    <?php
                    if ($_SESSION['TIPO_USER'] == 1 or $_SESSION['TIPO_USER'] == 2) {                             
                    ?>
                    <li class="dropdown">
                        <a href="ayuda">
                            <i class="fa fa-question-circle-o" aria-hidden="true"> </i>
                            Ayuda
                        </a>
                    </li>
                    <?php
                    }
                    ?>
                    
                    <li class="dropdown">
                      <?php echo "<a href='?modo=desconectar'><i class='fa fa-sign-out' aria-hidden='true'></i> Cerrar</a>"?>
                    </li>
                     
                    <li>
                        <div class="chip">
                            
                            <p style="color:#fff; margin-left: 120px; margin-top: 15px;" class="">
                                <?php echo $_SESSION['NOMBRE_USUARIOS'] ;?>
                            </p>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
</nav>
<div>
<?php include 'php/cierrosesion.php'; */?>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="index" class="nav-link">Inicio</a>
            </li>
            </ul>
        </nav>
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="index" class="brand-link">
            <img src="../images/logos/logo-diapo.png"
                alt="AdminLTE Logo"
                class="brand-image img-circle elevation-3"
                style="opacity: .8; display:block;border-radius:0;">
            </a>
            <br>
            <p class="brand-link">    
                <span class="brand-text font-weight-light">Ferretería la Campana</span>
            </p>
            <!-- Sidebar -->
            <div class="sidebar">
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
                </div>
                <div class="info">
                <a href="#" class="d-block"><?php echo $_SESSION['NOMBRE_USUARIOS'] ;?></a>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Registros
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="productos" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Artículos</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="categorias" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Categorías</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="estados" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Estado de Pedido</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="proveedores" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Proveedores</p>
                        </a>
                    </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-copy"></i>
                    <p>
                        Procesos
                        <i class="fas fa-angle-left right"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="pedido" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Pedido</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="requisicion" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Requisición</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="recepcion" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Recepción</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="consultarestadopedido" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Estatus de pedido</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="movimiento_inventario" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Movimiento de inventario</p>
                        </a>
                    </li>
                    
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-chart-pie"></i>
                    <p>
                        Tablas/Listados
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="productos_list" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Artículos</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="user_list" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Usuarios</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="consultarestados" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Estados</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="consultarproveedores" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Proveedores</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="consultarcategorias" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Categorías</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="consultarclientes" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Clientes</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="consultarpedidos" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Pedidos</p>
                        </a>
                    </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas fa-tree"></i>
                    <p>
                        Mantenimiento
                        <i class="fas fa-angle-left right"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <li class="nav-item">
                        <a href="respaldarbasededatos" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Respaldar Datos</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="restaurarbasededatos" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Restaurar Datos</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="usuarios" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Usuarios</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="auditoria_usuarios" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Auditoria de Usuarios</p>
                        </a>
                    </li>
                    </ul>
                </li>

                <li class="nav-header">AYUDA</li>
                <li class="nav-item">
                    <a href="ayuda" target="_blank" class="nav-link">
                        <i class="nav-icon fas fa-file"></i>
                        <p>Manual</p>
                    </a>
                </li>
                
                <li>
                    <a href="?modo=desconectar">
                        <i class="fa fa-circle-o text-red"></i> 
                        <span>Cerrar Sesion</span>
                    </a>
                </li>
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
  <?php include 'php/cierrosesion.php'; ?>
