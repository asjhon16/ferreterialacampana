<?php require"php/conexion.php";?>
<!DOCTYPE html>
<?php include'include/head.html';?>
<body>
<?php include 'include/menu.php'; ?>  

<div class="content-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h3><p align="center">PEDIDO N° <?php echo $id = $_REQUEST['id'] ?></p></h3>
                <h5>Fecha de Vencimiento: <?php
                $sql = "SELECT FECHA_EXPIRACION FROM pedidos WHERE ID_PEDIDO = '$id'";
                $query = mysqli_query($conexion, $sql);
                $pedido = mysqli_fetch_array($query);
                echo $pedido["FECHA_EXPIRACION"]
                ?></h5>
                <form action="reportes/notapedidos.php?nope=<?php echo $_REQUEST['id']; ?>" method="POST" class="datagrid">
                    <table>
                        <thead>
                        <tr>
                            <th>Codigo Articulo</th>
                            <th>Descripcion del articulo</th>
                            <th>Categoria</th>
                            <th>Cantidad Solicitada</th>
                            <th>Precio Unitario</th>
                            <th>Precio</th>
                        </tr>
                        </thead>
                        <?php

                            $sql = "SELECT detalles_pedido.ID_PRODUCTO, detalles_pedido.PRECIO, productos.DESCRIPCION_PRODUCTO, categorias.DESCRIPCION_CATEGORIAS, detalles_pedido.CANTIDAD_PRODUCTO 
                                        FROM detalles_pedido 
                                            INNER JOIN (
                                                productos INNER JOIN categorias ON productos.ID_CATEGORIAS=categorias.ID
                                            ) ON detalles_pedido.ID_PRODUCTO = productos.ID 
                                                WHERE detalles_pedido.ID_PEDIDO=".$_REQUEST['id'];

                            $consulta=mysqli_query($conexion,$sql);
                            $subtotal = 0;           
                            while($resgistros=mysqli_fetch_array($consulta)) {
                                $subtotal += $resgistros["CANTIDAD_PRODUCTO"]*$resgistros["PRECIO"];
                                echo'<tr>
                                <td>'.$resgistros["ID_PRODUCTO"].'</td>
                                <td>'.$resgistros["DESCRIPCION_PRODUCTO"].'</td>    
                                <td>'.$resgistros["DESCRIPCION_CATEGORIAS"].'</td>
                                <td>'.$resgistros["CANTIDAD_PRODUCTO"].'</td>
                                <td>'.$resgistros["PRECIO"].'</td>
                                <td>'.$resgistros["CANTIDAD_PRODUCTO"]*$resgistros["PRECIO"].'</td>
                                </tr>';
                            }
                            $iva = $subtotal*0.16;
                            echo '<tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>Subtotal</b></td>
                                    <td>'.$subtotal.'</td>
                                </tr>
                                
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>Iva</b></td>
                                    <td>'.$iva.'</td>
                                </tr>
                                
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><b>Total</b></b>
                                    <td>'.$total = $subtotal+$iva.'</td>
                                </tr>';
                            $stock=$resgistros["STOCK"];
                            ?>
                    </table>
                    <br>
                    <button style="position: relative; left: 45%; right: 45%;" type="submit" name="imprimir" class="btn btn-primary btn-sm">Exportar PDF</button>
                </form>
            </div>
        </div>
    </div>
<hr>
<?php include 'include/piedepagina.php';?>
</div>
<?php include'include/script.html';?>
</body>
</html>