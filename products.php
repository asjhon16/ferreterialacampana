<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1" name="viewport">
<link rel="canonical" href="promociones-articulos-ferreteria.html" />
<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
<link rel="manifest" href="manifest.json">
<link rel="mask-icon" href="safari-pinned-tab.svg">
<meta name="theme-color" content="#ffffff">
<title>Productos | Ferretería la Campana</title>
<meta name="keywords" content="" />
<meta name="description" content="Conoce los productos que tenemos actualmente en artículos de trabajo, hogar, material de construcción... y mucho más!" />
<!-- Metas Open Graph -->
<link rel="image_src" href="share.jpg" />
<meta property="og:image" content="share.jpg"/>
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="200" />
<meta property="og:image:height" content="200" />
<meta property="og:url" content="promociones-articulos-ferreteria.html" />
<meta property="og:title" content="Productos | Ferretería la Campana" />
<meta property="og:description" content="Conoce los productos que tenemos actualmente en artículos de trabajo, hogar, material de construcción... y mucho más!" />
<!-- Metas Facebook -->
<!-- Twitter Card data -->
<meta name="twitter:title" content="Productos | Ferretería la Campana">
<meta name="twitter:description" content="Conoce los productos que tenemos actualmente en artículos de trabajo, hogar, material de construcción... y mucho más!">
<meta name="twitter:image" content="share.jpg">
<link href="css/layout.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" media="print" href="css/print.css">
<script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="js/functions.js" type="text/javascript"></script> 
<!-- Cargamos nuestros scripts generales -->
<!-- Google Tag Manager -->
</head>

<body class="es">
<?php include 'menu.php'?>
<div class="generalContent">
<section class="block1 background">
<div class="container">
	<h1 class="with-hr-right text-right" style="color:#fff">Nuestros productos</h1>
			<div class="text-right"><p class="subtitle"></p></div>
</div>
</section>


<section class="block2 section-top-grey">
<div class="container">

	<div class="promoslist goup row">
		<ul>
		<li class="col-xs-12 col-sm-12 col-md-6"><div class="contenttext sameheight_products"><div class="innercontenttext"><h3 class="title">VERANO FERRECAMPANA 2018</h3><div class="textintro">Valida hasta: 24 de Junio 2018</div><a href="downloads/verano-ferbric-2018.pdf" class="btn btn-link"><span class="icon-download">Descargar folleto</span></a></div></div><div class="contentfoto sameheight_products"><img src="images/promociones/th_crop_1527241635verano-ferbric-2018.jpg" alt="VERANO FERRECAMPANA 2018" class="img-responsive center-block" /></div></li>

		<li class="col-xs-12 col-sm-12 col-md-6"><div class="contenttext sameheight_products"><div class="innercontenttext"><h3 class="title">PRIMAVERA FERRECAMPANA 2018</h3><div class="textintro">Valida hasta: 15 de Julio 2018</div><a href="downloads/primavera-ferbric-2018.pdf" class="btn btn-link"><span class="icon-download">Descargar folleto</span></a></div></div><div class="contentfoto sameheight_products"><img src="images/promociones/th_crop_1521110485primavera-ferbric-2018.jpg" alt="PRIMAVERA FERRECAMPANA 2018" class="img-responsive center-block" /></div></li>		</ul>
	</div>

</div>
</section>

<section class="home-ventajas">
	
</section>
<?php include 'pie.php'?>
</div>
<!-- end generalContent -->
</body>
</html>

