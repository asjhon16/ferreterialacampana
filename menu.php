<header>
	<div id="logo-wrapper">
		<div class="container">
	    	<div id="logo">
				<a href="index.html" title="">
	    		<img src="images/logos/logo.png" alt="" />
	    	</a>
			</div>
			<a href="login" title="" class="btn btn-submit" id="btn-asociados">Iniciar Sesión</a>	
		</div>
	</div>
	<!-- Logo -->
	<!-- Main navigation -->
	<div id="main-navigation">
		<span class="menu-icon"></span>
		<div class="main-navigation-content">
			<div class="container">
				<!-- Language nav
		        <div id="language-nav">
		            <span  class="langActive">es</span>
		            <ul>
		                <li class="active"><span>ES</span></li>
		            </ul>
		        </div>
			   End Language nav --> 
			   <nav id="nav">
					<ul>
						<li class="<?php echo basename($_SERVER['PHP_SELF'])  == 'index.php' ? 'current' : ''?>"><a href="index" title="ferretería de calidad en maracay">Inicio</a></li>
						<li class="<?php echo basename($_SERVER['PHP_SELF'])  == 'who.php' ? 'current' : ''?>"><a href="who" title="herretería de calidad">Quiénes somos</a></li>
						<!--li ><a href="servicios-cadena-ferreterias.html" title="cadena de compras ferreterías ">Registro</a></li-->
						<li class="<?php echo basename($_SERVER['PHP_SELF'])  == 'products.php' ? 'current' : ''?>"><a href="products" title="herramientas de trabajo">Productos</a></li>
						<!--li ><a href="establecimientos-ferreterias.html" title="tiendas de ferreterías España">Registro</a></li-->
						<li class="<?php echo basename($_SERVER['PHP_SELF'])  == 'register.php' ? 'current' : ''?>"><a href="register" title="registro ferretería la campana">Registro</a></li>
						<!--li ><a href="proveedores-ferreterias-bricolaje.html" title="proveedor de ferreterías y bricolaje">Proveedores</a></li-->
						<!--li ><a href="noticias-cadena-ferreterias.html" title="Noticias de ferreterías y bricolaje">Noticias</a></li-->
						<!--<li class="<?php //echo basename($_SERVER['PHP_SELF'])  == 'contact.php' ? 'current' : ''?>"><a href="contact" title="contactar con ferreterías">Contacto</a></li-->
					</ul>
				</nav>
				<div class="tel">
					<span class="icon-phone">0243 - 2378756</span>
				</div>
			</div>
		</div>
	</div>
<!-- Main navigation -->
</header>