<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta content="width=device-width, initial-scale=1.0, minimum-scale=1" name="viewport">
	<link rel="canonical" href="index.html" />
	<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
	<link rel="manifest" href="manifest.json">
	<link rel="mask-icon" href="safari-pinned-tab.svg">
	<meta name="theme-color" content="#ffffff">
	<title>Ferreteria la Campana</title>
	<meta name="keywords" content="" />
	<meta name="description" content="Ferretería la Campana es una ferretería que vende productos y artículos de trabajo desde jardinería, material de construcción y del hogar. " />
	<!-- Metas Open Graph -->
	<link rel="image_src" href="share.jpg" />
	<meta property="og:image" content="share.jpg"/>
	<meta property="og:image:type" content="image/jpeg" />
	<meta property="og:image:width" content="200" />
	<meta property="og:image:height" content="200" />
	<meta property="og:url" content="index.html" />
	<meta property="og:title" content="Ferretería la Campana | Ferretería de calidad en maracay
	" />
	<meta property="og:description" content="Ferretería la Campana es una ferretería que vende productos y artículos de trabajo desde jardinería, material de construcción y del hogar. " />
	<meta name="twitter:title" content="Ferretería la Campana | Ferretería de calidad en maracay
	">
	<meta name="twitter:description" content="Ferretería la Campana es una ferretería que vende productos y artículos de trabajo desde jardinería, material de construcción y del hogar.">
	<meta name="twitter:image" content="share.jpg">
	<link href="css/layout.css" rel="stylesheet" type="text/css" media="screen" />
	<link rel="stylesheet" media="print" href="css/print.css">
	<script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
	<script src="js/functions.js" type="text/javascript"></script> <!-- Cargamos nuestros scripts generales -->
<!--[if lt IE 9]>
    <script src="js/html5.js"></script>
<![endif]-->
</head>
<body class="home es">
<!--div id="cookies_msg_subwrapper">
   Utilizamos cookies propias y de terceros para ofrecer nuestros servicios y recoger datos estadísticos. Continuar navegando implica su aceptación.       
   <a href="cookies.html" title="política de cookies">Más información</a>
   <span id="cookies" class="cookies_msg_button" >Aceptar</span>
</div-->
<?php include 'menu.php'?>
<div class="generalContent">
	<!-- Carrousel -->
	<div class="owl-container clearfix">
		<div class="owl-carousel mainslider-full" id="owl-home">
			<div class="item">
				<a href="#">
					<img src="pics_fotosslide/1/esslide1.jpg" alt="">
				</a>
				<div class="claim-container container">
					<div class="inner-claim-container">
						<em class="h1like">
							<span>Venta de productos de ferretería,<br/> bricolaje, jardín, menaje, hogar y<br/> material de construcción</span>
						</em>
						<div class="text">
							<span></span>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<a href="#"
				>
				<img src="pics_fotosslide/1/esslider2.jpg" alt="">
			</a>
			<div class="claim-container container">
				<div class="inner-claim-container">
					<em class="h1like">
						<span>Venta de productos de ferretería,<br/> bricolaje, jardín, menaje, hogar y<br/> material de construcción</span>
					</em>
					<div class="text">
						<span></span>
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
<!-- End carrousel -->
<!-- Content -->
<section class="block2 section-top-grey section-bottom-grey" style="padding: 10px 0 10px 0">
<!--div class="container">
	<h2 class="with-hr-center">Productos</h2>
	<div class="text-center"><p class="subtitle">Infórmese de nuestras promociones en artículos de ferretería, bricolaje,<br/> jardinería, menaje, hogar y material de construcción</p></div>
	<div class="promoslist row">
		<ul>
		<li class="col-xs-12 col-sm-12 col-md-6"><div class="contenttext sameheight_products"><div class="innercontenttext"><h3 class="title">VERANO FERRETERIA LA CAMPANA 2.018</h3><div class="textintro">31 de Agosto 2.018</div><a href="downloads/verano-ferbric-2018.pdf" class="btn btn-link"><span class="icon-download"></span></a></div></div><div class="contentfoto sameheight_products"><img src="images/promociones/th_crop_1527241635verano-ferbric-2018.jpg" alt="VERANO FERRETERIA LA CAMPANA 2.018" class="img-responsive center-block" /></div></li><li class="col-xs-12 col-sm-12 col-md-6"><div class="contenttext sameheight_products"><div class="innercontenttext"><h3 class="title">PRIMAVERA FERRETERIA LA CAMPANA 2.018</h3><div class="textintro">31 de Agosto 2.018</div><a href="downloads/primavera-ferbric-2018.pdf" class="btn btn-link"><span class="icon-download"></span></a></div></div><div class="contentfoto sameheight_products"><img src="images/promociones/th_crop_1521110485primavera-ferbric-2018.jpg" alt="PRIMAVERA FERRETERIA LA CAMPANA 2.018" class="img-responsive center-block" /></div></li>		</ul>
	</div>
	<div class="col-xs-12 text-center"><a href="promociones-articulos-ferreteria.html" title="ofertas en artículos de ferretería" class="btn btn-link btn-lg">Ver todas</a>
	</div>
</div-->
</section>
<section class="block1">
	<div class="container">
		<h2 class="with-hr-center" style="color:#fff;">¿Porqué Nosotros?</h2>
		<div class="block-third">
			<div class="third">
				<span class="icon-user"></span>
				<h3>Ahorras tiempo al hacer tus pedidos</h3>
				<p>Con tan sólo un click tu pedido será apartado y deberás retirarlo en menos de 24 horas.</p>				<!--a href="servicios-cadena-ferreterias.html" title="productos bricolaje para particulares y profesionales" class="btn btn-default btn-lg">Ver más</a-->
			</div>
			<div class="third third-big">
				<span class="icon-nail"></span>
				<h3>Puedes acceder con un dispositivo conectado a internet</h3>
				<p>Sin limitaciones. Desde una computadora, teléfono o tablet podrá ingresar al sistema . </p>				<!--a href="servicios-cadena-ferreterias.html" title="cadena de tiendas en ferretería" class="btn btn-default btn-lg">Ver más</a-->
			</div>
			<div class="third">
				<span class="icon-hammer"></span>
				<h3>Evita el viaje hasta nuestra tienda</h3>
				<p>Muchos de nosotros vivimos en las ocupacione de la vida diaria y para ayudate te permitimos que sepas si tienes tu artículo para evitarte pérdida de tiempo.</p>				<!--a href="servicios-cadena-ferreterias.html" title="venta productos ferretería" class="btn btn-default btn-lg">Ver más</a-->
			</div>
		</div>
	</div>
</section>

<section class="section-top-white">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-5">
				<img src="images/branding/home-about-us.jpg" alt="profesionales del bricolaje" class="img-responsive center-block">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-1">
				<h1 class="with-hr-left text-left">Tu mejor ferretería</h1>
				<p class="subtitle">Ferretería la Campana, C.A. es una tiendas dedicadas a la venta de productos de ferretería, artículos para el hogar, jardín, hogar, material de construcción y suministros.</p>
				<p>Nuestra misión: Dar el mejor servicio mediante la atención cordial a los clientes. Nos adaptamos a las necesidades y expectativas de los mismos con unos precios muy competitivos, aportando como valor añadido una cercanía y proximidad Real al cliente y una calidad en productos. </p>				<!--a href="quienes-somos.html" title="ferreteria la campana, profesionales del bricolaje" class="btn btn-submit btn-lg">Ver más</a-->
			</div>
			
		</div>
		
	</div>
</section>

<!--section class="mapcontent map nopadding">
	<div class="container">
		<div class="map-search clearfix">
			<h2 class="icon-gmapmarker">Encuentra tu tienda</h2>
			<p>Puede buscar su tienda de ferretería más cercana en el mapa de la izquierda o mediante el siguiente formulario</p>			
			<form id="formZipCode">
				<input type="text" name="zipCode" id="zipCode" placeholder="Introduce el código postal">
				<a href="javascript:void(0)" id="btnZipCode" class="btn btn-submit">Buscar</a>
			</form>
		</div>
	</div>
	<div class="map" id="map_canvas"></div>
</section-->

<section class="home-ventajas">
	<div class="container">
		<h2 class="with-hr-center">Nuestras ventajas</h2>
		<div class="text-center"><p class="subtitle">FERRETERERIA LA CAMPANA es una cadena de tiendas dedicadas a la venta de productos de ferretería</p></div>
		<div class="row">
			<div class="ventaja col-xs-12 col-sm-12 col-md-6">
				<span class="icon-reseller"></span>
				<h3>Acuerdos con proveedores</h3>
				<p>Tenemos acuerdos con los mejores proveedores del sector, consiguiendo los mejores precios para ti.</p>			
			</div>
			<div class="ventaja col-xs-12 col-sm-12 col-md-6">
				<span class="icon-box"></span>
				<h3>Consejo y apoyo a nuestros asociados</h3>
				<p>Tenemos una red de promotores que cubre todo el territorio nacional, junto con el soporte de los profesionales de nuestra Central.</p>			
			</div>
		</div>
	</div>
</section>
<?php include 'pie.php'?>
</div>
<!-- end generalContent -->
<script src="js/owl.carousel.min.js"></script>
<script>
	$(document).ready(function(){
		$('#owl-home').owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			autoplayTimeout: 3000,
			nav: false,
			dots: false,
			navText: ["",""]
		})
	});
</script>
</body>
</html>