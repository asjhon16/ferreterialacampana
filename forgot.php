<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Recuperar contraseña | Ferretería la Campan</title>
    <meta name="description" content="Área interna para los asociados de Ferbric" />

    <link href="css/layout.css" rel="stylesheet" type="text/css" media="screen" />

    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" sizes="57x57" href="404.html">
    <link rel="apple-touch-icon" sizes="60x60" href="404.html">
    <link rel="apple-touch-icon" sizes="72x72" href="404.html">
    <link rel="apple-touch-icon" sizes="76x76" href="404.html">
    <link rel="apple-touch-icon" sizes="114x114" href="404.html">
    <link rel="apple-touch-icon" sizes="120x120" href="404.html">
    <link rel="apple-touch-icon" sizes="144x144" href="404.html">
    <link rel="apple-touch-icon" sizes="152x152" href="404.html">
    <link rel="apple-touch-icon" sizes="180x180" href="404.html">
    <link rel="icon" type="image/png" href="favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="404.html" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="manifest.json">
    <link rel="mask-icon" href="safari-pinned-tab.svg">

    <script src="js/jquery-2.2.4.min.js"></script>
    <script src="js/axios.min.js"></script>
	<script src="js/sweetalert2.min.js"></script>

    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <style>
        #wrapper{background-color:#FFF;height:100vh;}
        .loginpanel{position: absolute; top:50%; left: 50%; width: 300px; height: 400px; margin-top: -200px; margin-left: -150px; text-align: center;}
        .loginpanel img{margin:0 auto 2em auto;}
        .loginpanel h1{font-size:1.6em; margin: 0 0 1em;padding:0;}
    </style>
</head>

<body>
    <div id="wrapper">
        <div id="content">
            <div class="loginpanel">

                <img width="100%" src="images/logos/logo.png" />
                <h1>&iquest;Olvidaste tu Password?</h1>

                
                    <form action="php/loginController.php"  role="form" id="login" method="POST">
                        <ul>
                            <li class="full">
                                <input type="text" data-name="Email" name="email" id="email" required placeholder="Email:" />
                            </li>
                            <li class="full">
                                <input type="button" onclick="recovery()" value="Recuperar" class="btn btn-default" name="ingresar" >
                            </li>
                            <li class="full"><a href="login" class="btn btn-default">Volver</a></li>
                        </ul>
                    </form>

                
            </div>
        </div>
    </div>

<script>
    function recovery() {
        let form = $('#login').serializeArray()
        console.log(form)
        let data = {}
        for (let i = 0; i < form.length; i++) {
            data[form[i].name] = form[i].value
            console.log($('input[name='+form[i].name+']').data('name'))
            if (form[i].value == "") {
                Swal.fire("El campo " + $('input[name='+form[i].name+']').data('name') + " es requerido")
                return false
            }
        }

        data.reset = false

        axios.post('php/forgotController.php', data)
        .then(res => {
            if (res.data.result) {
                window.location = 'questions.php'
            } else {
                Swal.fire('Estimado usuario!', res.data.msg, 'info')
            }
        })
        .catch(err => {
            Swal.fire('Lo sentimos', 'En estos momentos no podemos procesar su solicitud', 'warning')
        })
    }
</script>
</body>

</html>
<?php session_destroy(); ?>