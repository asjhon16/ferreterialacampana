<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1" name="viewport">
<link rel="canonical" href="quienes-somos.html" />
<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
<link rel="manifest" href="manifest.json">
<link rel="mask-icon" href="safari-pinned-tab.svg">
<meta name="theme-color" content="#ffffff">
<title>Ferreteria la Campana</title>
<meta name="keywords" content="" />
<meta name="description" content="Ferbric es una cadena de ferreterías que vende productos de bricolaje, jardinería, menaje, del hogar y material de construcción. ¡Infórmate!" />
<!-- Metas Open Graph -->
<link rel="image_src" href="share.jpg" />
<meta property="og:image" content="share.jpg"/>
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="200" />
<meta property="og:image:height" content="200" />
<meta property="og:url" content="quienes-somos.html" />
<meta property="og:title" content="Venta de artículos de bricolaje, menaje, hogar | Ferbric, cadena de ferreterías" />
<meta property="og:description" content="Ferbric es una cadena de ferreterías que vende productos de bricolaje, jardinería, menaje, del hogar y material de construcción. ¡Infórmate!" />
<!-- Metas Facebook -->
<!-- Twitter Card data -->
<meta name="twitter:title" content="Venta de artículos de bricolaje, menaje, hogar | Ferbric, cadena de ferreterías">
<meta name="twitter:description" content="Ferbric es una cadena de ferreterías que vende productos de bricolaje, jardinería, menaje, del hogar y material de construcción. ¡Infórmate!">
<meta name="twitter:image" content="share.jpg">
<link href="css/layout.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" media="print" href="css/print.css">
<script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="js/functions.js" type="text/javascript"></script> <!-- Cargamos nuestros scripts generales -->
<!--[if lt IE 9]>
    <script src="js/html5.js"></script>
<![endif]-->
</head>
<body class="es">
<?php include 'menu.php'?>
<div class="generalContent">

<section class="block1 background">
<div class="container">

	<h1 class="with-hr-right text-right">Quiénes somos</h1>
	<div class="text-right"><p class="subtitle" style="color:#fff;">Ferretería la Campana es una ferretería que provee articulos de trabajo<br /> en el ramo de las herramientas, jardín, menaje y
hogar, <br />material de construcción y suministros teniendo atención <br />de calidad para los diferentes clientes que se <br />acercan a nuestras instalaciones</p></div>
</div>
</section>

<section class="block2 section-top-grey">
<div class="container">
	<div class="row">

		<div class="col-xs-12 col-sm-12 col-md-5 goup">
			<img src="images/branding/quienes-somos-01.png" alt="Expertos en bricolaje, jardinería, material de construcción, jardinería y menaje" class="img-responsive center-block">
		</div>
		<div class="col-xs-12 col-sm-12 col-md-6 col-md-offset-1">
			<h2 class="with-hr-left text-left">Herramientas a tu alcance</h2>
			<p>Queremos que nuestra marca sea el fiel reflejo del trabajo en equipo orientado a la consecución de las mejores condiciones para incrementar las ventas y optimizar la rentabilidad de nuestros establecimientos Asociados.</p>
<p>Le ofrecemos gran variedad de productos, desde llaves, tubos, tuercas, taladros, pinturas y mucho más en un solo lugar de Maracay
.</p>
<p><strong>Nuestra misión:</strong>Proveer articulos de trabajo en el ramo de las herramientas, teniendo atención de calidad para los diferentes clientes que se acercan a nuestras instalaciones.</p>
<p><strong>Nuestros visión:</strong> Ser la Ferreteria líder en el mercado que provea la mayor cantidad de marcas y variedades de todos los tipos.</p>		
<p><strong>Nuestros valores:</strong> Trabajo en Equipo, Exigencia, Compromiso y una amplia experiencia en el sector.</p>
</div>

	</div>
</div>
</section>

<section class="block2 nopadding">

	<div class="half pull-right">
		<div class="img-frame">
			<img src="images/branding/quienes-somos-02.jpg" alt="EMPRESA_OBJETIVOS_ALT" class="img-responsive center-block">
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-6 halfpadding">
				<h2 class="with-hr-left text-left">Objetivos</h2>
				<p>.</p>
<p><strong>OBJETIVO GENERAL:</strong> Ser la opción del venezonalo en el ramo ferretero por excelencia.</p>
<p><strong>OBJETIVO ESPECIFICOS:</strong>.</p>
<ul>
	<li>Tener un inventario con los productos de más reconocimiento en el mercado por su calidad</li>
	<li>Establecer un software online que gestione de pedidos previo a su compra.</li>
	<li>Implemetar bonificaciones por producción, recreaciones y ambiente creativo a nuestros equipo de trabajo.</li>
	<li>Multiplicar sedes de Ferretería la campana en cada estado de Venezuela</li>
</ul>
<p>.</p>	
			</div>
		</div>
	</div>

</section>
<?php include 'pie.php'?>
</div>
<!-- end generalContent -->
</body>
</html>

