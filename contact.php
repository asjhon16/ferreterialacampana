<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8">
<meta content="width=device-width, initial-scale=1.0, minimum-scale=1" name="viewport">
<link rel="canonical" href="contacto.html" />
<link rel="apple-touch-icon" sizes="180x180" href="apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
<link rel="manifest" href="manifest.json">
<link rel="mask-icon" href="safari-pinned-tab.svg">
<meta name="theme-color" content="#ffffff">
<title>Ferretería la Campana</title>
<meta name="keywords" content="" />
<meta name="description" content="Envianos tus preguntas e inquietudes a nuestro correo por medio de nuestro formulario. Estaremos encantados de atenderte. " />
<link rel="image_src" href="share.jpg" />
<meta property="og:image" content="share.jpg"/>
<meta property="og:image:type" content="image/jpeg" />
<meta property="og:image:width" content="200" />
<meta property="og:image:height" content="200" />
<meta property="og:url" content="contacto.html" />
<meta property="og:title" content="Contáctanos | Ferretería la Campana" />
<meta property="og:description" content="Envianos tus preguntas e inquietudes a nuestro correo por medio de nuestro formulario. Estaremos encantados de atenderte. " />
<meta name="twitter:title" content="Contáctanos | Ferretería la Campana">
<meta name="twitter:description" content="Envianos tus preguntas e inquietudes a nuestro correo por medio de nuestro formulario. Estaremos encantados de atenderte. ">
<meta name="twitter:image" content="share.jpg">
<link href="css/layout.css" rel="stylesheet" type="text/css" media="screen" />
<link rel="stylesheet" media="print" href="css/print.css">
<script src="js/jquery-3.1.1.min.js" type="text/javascript"></script>
<script src="js/functions.js" type="text/javascript"></script> <!-- Cargamos nuestros scripts generales -->
<!--[if lt IE 9]>
    <script src="js/html5.js"></script>
<![endif]-->
</head>
<body class="es contact">
<?php include 'menu.php'?>
<div class="generalContent">

<section class="block1 background">
<div class="container">
	<h1 class="with-hr-right text-right"></h1>
</div>
</section>

<section class="section-top-grey block2">
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-8 col-md-offset-2 bigpadding goup block3">
			<h2 class="with-hr-center">Contáctanos</h2>
			<p class="text-center">Envíanos tus datos, consultas e inquietudes y nos pondremos en contacto contigo.</p>
			<span id="errores" class="col-xs-12 col-sm-12 alert alert-danger" style="display:none;"></span>
			<div class="row">
				<form onSubmit="return validarForm();" action="" class="form01" id="formulario" name="formulario" method="post" >

					<input type="hidden" name="form" id="form" value="form" />
					<input type="hidden" name="url" id="url" value="contacto-ok.html" />
					<input type="hidden" name="subject" id="subject" value="Envío de formulario de contacto Ferretería la campana" />

					<ul>
						<li class="col-xs-12 col-sm-12"><input name="Nombre" type="text" id="Nombre" placeholder="Nombre*" required/></li>
						<li class="col-xs-12 col-sm-12 col-md-6"><input name="Telefono" type="tel" id="Telefono" placeholder="Teléfono*" required/></li>
						<li class="col-xs-12 col-sm-12 col-md-6"><input name="Email" type="email" id="Email" placeholder="Correo*" required/></li>
						<li class="col-xs-12 col-sm-12"> <textarea name="Comentarios" id="Comentarios" placeholder="Comentarios*" required></textarea> </li>
						<li class="col-xs-12 col-sm-12"><input class="termsConditions" type="checkbox" name="terms" id="validateCheck" ><label  for='validateCheck'><span></span><a href="legal.html">Acepto términos y condiciones</a></label></li>
						
						<li class="col-xs-12 col-sm-12">
							<input type="hidden" name="iso_idioma" id="iso_idioma" value="es" />
							<div class="text-center"><input value="Enviar" class="btn btn-submit btn-lg submit" name="button" type="submit" id="button"></div>
						</li>
					</ul>
				</form>
			</div>

		</div>
	</div>
</div>
</section>

<section class="mapcontent map nopadding">
	<div class="map" id="map_canvas"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3926.1704808996246!2d-67.6008704860991!3d10.247830692681521!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e803c993c6bf457%3A0x35d78e8515908545!2sFerreter%C3%ADa+La+Campana!5e0!3m2!1sen!2sve!4v1528002237224" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe></div>
</section>
<?php include 'pie.php'?>
</div>
<!-- end generalContent -->
<script>
	$("input:text").focus(function(){
		if($(this).val() == $(this).attr('name')){
			$(this).val('');
		}
	});

	$("input:text").blur(function(){
		if($(this).val() == ""){
			$(this).val($(this).attr('name'));
		}
	});
</script>
</body>
</html>